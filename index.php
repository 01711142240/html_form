<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Bootstrap 101 Template</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

<!--      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->

    </head>
    <body style="background-color:#faebcc">
        <div class="container">
            <h1>U.S. Standard Certificate of Live Birth</h1>
            <div class="row">
                <div class="col-md-6">
                    <h3>Child's Details</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <form>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">First Name</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Middle Name</label>
                                <input type="email" class="form-control" id="exampleInputEmail1">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <form>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Last Name</label>
                                <input type="email" class="form-control" id="exampleInputEmail1">
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Suffix</label>
                            <input type="email" class="form-control" id="exampleInputEmail1">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Time of birth</label>
                            <input type="email" class="form-control" id="exampleInputEmail1">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <form>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Sex</label><br>
                                <select type="email" class="form-control" id="exampleInputEmail1">
                                    <option>Male</option>
                                </select>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Date of Birth</label>
                            <input type="email" class="form-control" id="exampleInputEmail1">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Facility Name(if not institution, give street and number)</label>
                            <input type="email" class="form-control" id="exampleInputEmail1">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">City, Town, or Location of Birth</label>
                            <input type="email" class="form-control" id="exampleInputEmail1">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Country of Birth</label>
                            <input type="email" class="form-control" id="exampleInputEmail1">
                        </div>
                    </div>
                    <h3>Father's Details</h3>
                    <p>FATHER'S CURRENT LEGAL NAME</p>
                    <div class="col-md-6">
                        <form>
                            <div class="form-group">
                                <label for="exampleInputEmail1">First Name</label>
                                <input type="email" class="form-control" id="exampleInputEmail1">
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Middle Name</label>
                            <input type="email" class="form-control" id="exampleInputEmail1">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <form>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Last Name</label>
                                <input type="email" class="form-control" id="exampleInputEmail1">
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Suffix</label>
                                <input type="email" class="form-control" id="exampleInputEmail1">
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Date of birth(Mo/Day/Yr)</label>
                            <input type="email" class="form-control" id="exampleInputEmail1">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Birthplace(State, Territory, or Foreign Country)</label>
                            <input type="email" class="form-control" id="exampleInputEmail1">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h3>Certifier Details</h3>
                        <label>Certifier's Name</label>
                        <input type="email" class="form-control" id="exampleInputEmail1">
                        <label>Title</label>
                        <input type="checkbox"> MD <input type="checkbox"> DO <input type="checkbox"> HOSPITAL ADMIN <input type="checkbox"> CNM/CM <input type="checkbox"> OTHER MIDWIFE<br><input type="checkbox"> OTHER (Specify)<br>
                        <input type="text" class="form-control" id="exampleInputEmail1">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="exampleInputEmail1">Date Certificate(Mo/Day/Yr)</label>
                                <input type="text" class="form-control" id="exampleInputEmail1">
                            </div>
                            <div class="col-md-6">
                                <label for="exampleInputEmail1">Date Field by Register(Mo/Day/Yr)</label>
                                <input type="text" class="form-control" id="exampleInputEmail1">
                            </div>
                        </div>
                        <label for="exampleInputEmail1">Telephone</label>
                        <input type="text" class="form-control" id="exampleInputEmail1">
                        <label for="exampleInputEmail1">UR#</label>
                        <input type="text" class="form-control" id="exampleInputEmail1">
                        <label for="exampleInputEmail1">Referring Hospital</label>
                        <input type="text" class="form-control" id="exampleInputEmail1">
                    </div>
                </div>
                <div class="col-md-6">
                    <h3>Mother's Details</h3>
                    <p>Mother's Current Legal Name</p>
                    <div class="row">
                        <div class="col-md-6">
                            <form>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">First Name</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Middle Name</label>
                                <input type="email" class="form-control" id="exampleInputEmail1">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <form>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Last Name</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <form>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Suffix</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Date of birth(Mo/Day/Yr)</label>
                                <input type="email" class="form-control" id="exampleInputEmail1">
                                <div class="row">
                                    <p>Mother's Name Prior to First Marriage</p>
                                    <div class="col-md-6">
                                        <label for="exampleInputEmail1">First name</label>
                                        <input type="email" class="form-control" id="exampleInputEmail1">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="exampleInputEmail1">Middle name</label>
                                        <input type="email" class="form-control" id="exampleInputEmail1">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="exampleInputEmail1">Last name</label>
                                        <input type="email" class="form-control" id="exampleInputEmail1">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="exampleInputEmail1">Suffix</label>
                                        <input type="email" class="form-control" id="exampleInputEmail1">
                                    </div>
                                </div>

                            </div>
                            <label for="exampleInputEmail1">Birthplace(State, Territory,or Foreign Country)</label>
                            <input type="email" class="form-control" id="exampleInputEmail1">
                            <label for="exampleInputEmail1">Residence of Mother-State</label>
                            <input type="email" class="form-control" id="exampleInputEmail1">
                            <label for="exampleInputEmail1">County</label>
                            <input type="email" class="form-control" id="exampleInputEmail1">
                            <label for="exampleInputEmail1">City, Town or Location</label>
                            <input type="email" class="form-control" id="exampleInputEmail1">
                            <label for="exampleInputEmail1">Street and Number</label>
                            <input type="email" class="form-control" id="exampleInputEmail1">
                            <label for="exampleInputEmail1">Apartment No.</label>
                            <input type="email" class="form-control" id="exampleInputEmail1">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="exampleInputEmail1">Zip Code</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1">
                                </div>
                                <div class="col-md-6">
                                    <label for="exampleInputEmail1">Inside City Limits?</label><br>
                                    <input type="radio" name="sex" value="male">Yes
                                    <input type="radio" name="sex" value="female">No
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>